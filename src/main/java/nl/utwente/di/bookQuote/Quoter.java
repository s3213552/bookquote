package nl.utwente.di.bookQuote;

import java.util.HashMap;
import java.util.Map;

public class Quoter {
public static Map<String, Double> books = new HashMap<>();

    public void putBooks() {
        books.put("1", 10.0);
        books.put("2", 45.0);
        books.put("3", 20.0);
        books.put("4", 35.0);
        books.put("5", 50.0);
        books.put("others", 0.0);
    }
    public double getBookPrice(String bookId) throws Exception {
        if (books.containsKey(bookId)) {
            return books.get(bookId);
        }
        else {
            throw new Exception("Book not found");
        }
    }
}
