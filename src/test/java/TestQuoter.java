import nl.utwente.di.bookQuote.Quoter;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;

public class TestQuoter {
    @Test
    public void testBook1() throws Exception {
        Quoter quoter = new Quoter();
        quoter.putBooks();
        double price = quoter.getBookPrice("1");
        Assertions.assertEquals(10.0, price,0.0, "Price of book 1");
    }
}
